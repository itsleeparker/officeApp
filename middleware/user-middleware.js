const Users = require("../userModel").users;
const bcrypt = require("bcrypt");

const saltrounds = 10;

const registerUser = (req , res , next)=>{
	Users.findOne(
	{uid : req.body.uid }, 
	(err , results)=>{
	if(results){
		res.render('fetch' , 
			{
				user : results  ,
				errmsg : "User Already exists"
			});
	}else{
		bcrypt.hash(req.body.pwd , saltrounds , (err , hash)=>{
			if(!err){
				const newUser = new Users({
					uid    : req.body.uid,
					uname  : req.body.fname,
					lname  : req.body.lname,
					mobile : req.body.mobile,
					email  : req.body.email,
					pwd    : hash,
					Admin  : false,
					Manager : false
				});
				newUser.save(err=>{
					if(!err){
			   			req.user = newUser._id;
			   			next();
					}else{
						res.send("Something went wrong");
					}
				});
			}
		})
	}
	})
}


const isAdmin = (req   , res , next)=>{
	const uid  = req.params.uid;
	Users.findOne({_id : uid} , (err ,results)=>{
		if(results){
			if(results.Admin){
				req.Admin = true;
				next();
			}else{
				req.Admin = false;
				next();
			}
		}else{
			res.send("Error While looking for user");
		}
	})
}

const isManager = (req   , res , next)=>{
	const uid  = req.params.uid;
	Users.findOne({_id : uid} , (err , results)=>{
		if(results){
			if(results.Manager){
				req.Manager = true;
				next();
			}else{
				req.Manager = false;
				next();
			}
		}else{
			res.send("Error while looking for user")
		}
	})
}

const verifyUser = (req ,res , next)=>{
	const uname = req.body.uname;
	const pwd = req.body.pwd;
	Users.findOne({uid : uname} , (err , results)=>{
		if(results){
			//used to decrypt the password for the user
			bcrypt.compare(pwd, results.pwd, function(err, password) {
   				 if(password){
   				 	req.user = results._id;
					next();
   				 }else{
					res.render("homepage" , {errmsg : "Wrong Password"})
				}
			});
		}else{
			res.render("homepage" , {errmsg : "User not found"});
		}
	})
}

module.exports = {
	register :registerUser,
	verify : verifyUser,
	admin : isAdmin,
	manager : isManager
}