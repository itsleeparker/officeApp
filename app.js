require("dotenv").config();
const express  = require('express');
const sessions = require("express-session");
const cookieParser = require("cookie-parser");
const http= require('http');
const fs = require('fs');
const path = require('path');
const bodyParser = require('body-parser');
const ejs  = require('ejs');
const mongoose = require('mongoose');
const app =express();
const sessionCofig = require('./middleware/session-handler').config
const userMiddleware = require("./middleware/user-middleware")
app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended : true}));
app.use(sessions(sessionCofig));
app.use(cookieParser());
app.set('view engine' , 'ejs');


////////////////ROUTING FILES/////////////////////////////////
const homeRoute = require("./routes/homeRoute");
const registerRoute = require("./routes/register-routes");
const fetchRoute = require("./routes/fetch-route.js");
const additionalRoute = require("./routes/additional-route");
const profileRoute = require("./routes/profile-route");
const userRoute = require("./routes/user-route");
const adminRoute = require("./routes/admin-route");
const messageRoute = require("./routes/message-route");
const pdfRoute = require("./routes/pdfGenerator-route");
const userSettingsRoute = require("./routes/userSettingRoute");
const eventRoutes = require("./routes/eventRoutes");
//////////////SET THE MIDDLEWARE FOR IMAGES///////////
const upload = require("./image-handler").users;

//////////////////DATABASE HANDLING////////////
mongoose.connect("mongodb+srv://"+process.env.DB+process.env.DB_NAME);


//////////////////route handling///////////////
app.use(homeRoute.routes);
app.use(registerRoute.routes);
app.use(fetchRoute.routes);
app.use(additionalRoute.routes);
app.use(adminRoute.routes);
app.use(profileRoute.routes);
app.use(userRoute.routes);
app.use(messageRoute.routes);
app.use(pdfRoute.routes);
app.use(userSettingsRoute.routes);
app.use(eventRoutes.routes);
/////////////////WEB SOCKETS///////////////////////


////////////////PORT HANDLING///////////////////
let PORT = process.env.PORT;
if(PORT == '' || PORT == null){
	PORT = 3000;
}

app.listen(PORT , (err)=>{
	if(!err){
		console.log('Server Live at Port 3000');
	}
});

module.exports = (user)=>{
	io.on("connection" , (socket)=>{
		console.log("User Connected")
	})
};