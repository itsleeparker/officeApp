const express = require("express");
const fs = require("fs");
const path = require("path");
const app = express.Router();
const Users = require('../userModel.js').users;
const upload = require("../image-handler");

app.route('/additional/:user')
.get((req , res)=>{
	const user  = req.params.user;
	Users.findById(user  , (err , results)=>{
		if(results){
 		  res.render("additional" , {
 		  	user : results
	      });
		}
	})

})
.post(upload.single("image"),(req ,res)=>{
	const user = req.body.uid;
	//if the user does not upload any image upload the default image 
	if(req.file != null){
			var img = {
		name  : req.file.filename,
		img : {
			data : fs.readFileSync(path.join(__dirname , '..','Images' , req.file.filename)),
			contentType : 'image/png'
		}
	};
	}else{
			var img = {
				name  : "default",
				img : {
					data : fs.readFileSync(path.join(__dirname ,'..' ,'Images','default.png')),
					contentType : 'image/png'
				}
			};
	}
	Users.findById(user , (err , results)=>{
		if(results){
			const additional = {
				dob : req.body.dob,
				imgData : img
			}
			Users.findOneAndUpdate({_id : results._id} , {add :additional} ,(err , results)=>{
				if(!err){
					console.log("Image Uploaded successfully!");
					res.redirect('/users/'+results._id);
				}
			});
		}else{
			res.send("Something Went Wrong 1");
		}
	})

})


module.exports = {
	routes : app
}