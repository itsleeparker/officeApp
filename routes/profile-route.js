const express = require("express");
const fs = require("fs");
const path = require("path");
const app = express.Router();
const puppeteer = require("puppeteer");
const Users = require('../userModel.js').users;
const upload = require("../image-handler");


app.get("/profile/:user/:edit",(req ,res)=>{
	const uid = req.params.user;
	const edit = req.params.edit;
	Users.findOne({_id : uid} , (err ,emp)=>{
		if(!err){
			if(edit == "true"){
				res.render("profile" , {
					user : emp,
					edit : "imgVal",
					btn  : "Edit Profile",
					errmsg: ""
				});				
			}else{
				res.render("profile" , {
					user : emp,
					edit : "",
					btn  : "Share Profile",
					errmsg: ""
				});
			}
			
			}else{
				res.send("Error finding the user pls try again later");
		}
	});
})
app.post("/profile/:uid" ,upload.single("image"), (req , res)=>{
	//upload the new data updated by the user
	const uid = req.params.uid;
	const btn  = req.body.profileBtn;

	const userAddress = {
		localAddress : req.body.local_address,
		country  : req.body.country,
		city     : req.body.city,
		zip      : req.body.zip		
	}

	if(btn  == "Edit Profile"){
			Users.findById(uid , (err , results)=>{		
			if(results){
				//check if user has uploaded new image or not 
				if(req.file == null){
					var img = {
						name  : results.add.imgData.name,
						img : {
							data : results.add.imgData.img.data,
							contentType : results.add.imgData.img.contentType
						}
					}			
					var addData = {
						address : userAddress,
						imgData : img
					}
				}else{
					var img = {
						name  : req.file.filename,
						img : {
							data : fs.readFileSync(path.join(__dirname + '/Images/' + req.file.filename)),
							contentType : 'image/png'
						}
					}
					var addData = {
						address : userAddress,
						imgData : img
					}		
				}
				const newuser  = {
				uid    : req.body.uid,
				uname  : req.body.fname,
				lname  : req.body.lname,
				mobile : req.body.mobile,
				email  : req.body.email,
				add    : addData
				}			
				Users.findOneAndUpdate({_id : results._id } , newuser , (err, result)=>{
					if(!err){
						res.redirect("/users/"+result._id);
					}else{
						console.log(err);
					}
				})
			}else{
				res.send("error");
			}
		})
	}else{
		//CODE FOR SHARING THE PROFILE AS A PDF OR THROUGH DM OR EMAIL ADDRESS
		(async ()=>{
	  			const browser = await puppeteer.launch();
	  			const page  = await browser.newPage();
	  			await page.goto('http://localhost:3000/pdf/'+req.params.uid);
	  			await page.pdf({path : "employee.pdf" , printBackground: true});			
	  			console.log("File generated");
	  			res.redirect("/pdf/"+uid);
		})();
		
	}
});


module.exports = {
	routes : app
}