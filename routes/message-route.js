const express = require("express");
const fs = require("fs");
const path = require("path");
const app = express.Router();
const Users = require('../userModel.js').users;
const Chat = require('../userModel.js').chat

app.route("/message/:uid")
.get((req , res)=>{
	const sender = req.params.uid;
	Users.find({} , (err , results)=>{		
		if(results){
			res.render("message" , {user : results  , sender  : sender});
		}
	})
	
})

app.route("/chat/:sender/:uid")
.get((req , res)=>{
	const uid = req.params.uid;
	const sender = req.params.sender;
	if(uid == "blank"){
		res.render("messageBlank");
	}
	Users.findById(uid , (err , results)=>{
		if(results){
			Chat.findOne({owner1 : sender , owner2 : uid}, (err , chat_results)=>{
				if(chat_results){
					res.render("chat", {user : results , sender : sender, chat_id  : chat_results._id});
				}else{
					res.render("chat", {user : results , sender : sender, chat_id  : ""});					
				}
			})
			
		}
	})
})

.post((req ,res)=>{
	const sender  = req.params.sender;
	const rec = req.params.uid;
	const message = req.body.message;
	const chat_id = req.body.chat_id;

	console.log(chat_id);
	//try keeping set backs our here later point using passport
	const newMessage = {
		send : sender,
		rec : rec,
		message : message
	};
	const userMessage = JSON.stringify(newMessage);

	//find if the users have any message or not 
	Chat.findById(chat_id, (err , results)=>{
		if(results){
			results.chats.push(userMessage);
			Chat.findOneAndUpdate({_id : results._id} , {
				owner1: results.owner1,
				owner2 : results.owner2,
				chats : results.chats
			} , (err , out)=>{
				if(err){
					console.log("Message Sent");
				}
			});
		}else{
			const messageArray = [];
			messageArray.push(userMessage);
			const newChat = new Chat({
				owner1  : sender,
				owner2  : rec,
				chats : messageArray
			});

			newChat.save((err)=>{
				if(!err){
					console.log("Chat Saved");
					console.log(newChat);
				}
			})
		}
	})

})

module.exports = {
	routes : app
}