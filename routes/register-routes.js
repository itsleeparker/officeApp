const express = require("express");
const app = express.Router();
const Users = require('../userModel.js').users;
app.route('/register')
.get((req  , res)=>{
	res.render('register' , {errmsg : ""});
})
.post((req , res)=>{
	const uid = req.body.uid;
	const btn  = req.body.btn;
		Users.findOne({uid :uid} , (err , results)=>{
		  if(results){
				res.redirect("/additional/"+results._id);
			}else{
				res.render("register" , {errmsg : "User not found"});
			}
		})
})

module.exports = {
	routes : app
}