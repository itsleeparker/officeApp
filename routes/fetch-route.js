const express = require("express");
const app = express.Router();
const Users = require('../userModel.js').users;
const register = require("../middleware/user-middleware.js").register;
const logIn = require("../middleware/session-handler").userLogin;
app.route('/fetch/:user')
.get((req , res)=>{
	const userData  = req.params.user;
	res.render("fetch" , {user : userData , errmsg : ''});
})

app.post('/fetch',register,logIn,(req , res)=>{
	//check if username already exisist
	const user = req.user;
	res.redirect(`/additional/${user}`);
})


module.exports = {
	routes : app
}