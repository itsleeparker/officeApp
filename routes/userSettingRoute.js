const express = require("express");
const app = express.Router();
const Users = require('../userModel.js').users;
const isLoggedIn = require("../middleware/session-handler").log;
const isAdmin = require("../middleware/user-middleware").admin;
const isManager = require("../middleware/user-middleware").manager;
const _ = require("lodash").camelCase;
app.route("/user-settings/:uid")

//check if the user has any admin or manager privileges and dispolay user setting accordingly
.get(isLoggedIn , isAdmin , isManager ,(req , res)=>{
    admin = req.Admin;
    manager = req.Manager;
    // console.log("Verified")
    if(!req.logged){
        res.redirect('/');
    }
    options = ["Delete Account"];
    //set the options fro the user for the setting options as a array according to the user privliges
    if(admin){
        options.push("Admin Request");
        options.push("Manager Request");
        options.push("Delete User");
        options.push("Edit User");   
        options.push("Manage Events");
        options.push("Add member to Events"); 
    }else if(manager){
        options.push("Manage Events");
        options.push("Add member to Events");
        options.push("Contact Admin");
        // options.push("Apply for Admin");
    }else{
        options.push("Contact Admin");
    }

    //Redner the user page according to the user 
    Users.findOne({_id : req.params.uid} , (err , results)=>{
        if(results){
              res.render("settings" , 
              {
                  user : results,
                  op  : options
              })  
        }else{
            res.send("User Not Found Please try again later");
        }
    })  
})

app.get( "/setting/:options" , isLoggedIn  , isAdmin , isManager , (req  , res)=>{
    option = _(req.params.options);
    console.log(option);
    const admin = req.Admin;
    const manager = req.Manager;
    if(!req.logged){
        res.redirect("/");
    }
    // WRITE CODE HERE FOR USER SETTING OPTIONS 
    if(option == "deleteAccount"){

    }else if(option == "adminRequest"){

    }else if(option == "managerRequest"){

    }else if (option == "deleteUser"){

    }else if(option == "editUser" && admin){

    }else if(option =="manageEvents"){

    }else if(options == "addMemberToEvents" && manager){

    }else{

    }
})


module.exports = {
    routes : app
}