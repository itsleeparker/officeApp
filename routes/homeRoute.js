const express = require("express");
const app = express.Router();
const Users = require('../userModel.js').users;
// const session = require("express-session")
const isLoggedIn = require("../middleware/session-handler").log;
const logIn = require("../middleware/session-handler").userLogin;
const verify = require("../middleware/user-middleware").verify;
const logout = require("../middleware/session-handler").logout;
app.route('/')

.get(isLoggedIn , (req ,res)=>{
	const user = req.user;
	if(!req.logged){
		res.render("homepage" , {errmsg : ""});
	}else{
		res.redirect(`/users/${req.user}`);
	}
})

.post(verify,logIn,(req ,res)=>{
	const user = req.user;
	res.redirect(`/users/${user}`);		
});

app.get("/logout" , logout);

module.exports = {
	routes : app
}