const express = require("express");
const Users = require('../userModel').users;
const path = require('path');
const puppeteer = require('puppeteer');
const app = express.Router();

app.get("/pdf/:uid" , (req ,res)=>{
	const uid = req.params.uid;
	Users.findById(uid  , (err , results)=>{
		if(results){
			res.render("pdf" , 
				{
					user : results,
					filePath : "/download/pdf"
				}
				);
		}
	})
});

app.get("/download/pdf" , (req , res)=>{
	const filepath = path.join(__dirname , '../');
	const fileName = "employee.pdf"

	res.download(filepath+fileName , fileName , err=>{
		if(err){
			res.status(500).send({
				message : "Not able to download file"
			})
		}
	})
})
module.exports = {
	routes : app
}