const express = require("express");
const app = express.Router();
const Users = require('../userModel.js').users;

app.route("/admin/:option/:uid")
.get((req , res)=>{

	//code for displaying the side admin panel and manipulating each option accordingly
	const op = req.params.option;
	if(op == "userlist"){
		Users.find({} , (err , results)=>{
			if(results){
				res.render(op , {
					emp : results
				})
			}else{
					res.send("Something went wrong 1");
				}
		})
	}
})
.post((req , res)=>{
	
})


module.exports = {
	routes : app
}