const express = require("express");
const app = express.Router();
const Users = require('../userModel.js').users;
const isLoggedIn = require("../middleware/session-handler").log;
const isAdmin = require("../middleware/user-middleware").admin;
const isManager = require("../middleware/user-middleware").manager;


app.route("/events/:uid")

.get(isLoggedIn , isManager   , isAdmin , (req , res)=>{
    const admin = req.Admin;
    const manager = req.manager;
    if(!req.logged){
        res.redirect('/');
    }
    res.render("event");
})

module.exports = {
    routes : app
}